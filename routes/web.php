<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomepageController::class, 'index'])->name('homepage');

Route::get('/login', [App\Http\Controllers\AuthController::class, 'index'])->name('login');

Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile.index');

Route::get('/history', [App\Http\Controllers\HistoryController::class, 'index'])->name('history.index');
Route::get('/history/detail', [App\Http\Controllers\HistoryController::class, 'detail'])->name('history.detail');
