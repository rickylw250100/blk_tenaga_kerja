<x-homepage-master>
    @section('content')
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="mb-2 text-white">History Pekerjaan</h3>
              <hr class="mb-2 bg-white"/>
            </div>
            
            <div class="row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-3 row mx-auto">
              @for ($i = 0; $i < 10; $i++)
                <div class="col">
                  <div class="card">
                    <div class="row no-gutters">
                      <div class="col-md-3 my-auto">
                        <a href="#" class="avatar rounded-circle mr-3 mx-auto d-block rounded">
                          <img alt="Image placeholder" src="{{asset("img/theme/bootstrap.jpg")}}">
                        </a>
                      </div>
                      <div class="col-md-9">
                        <div class="card-body">
                          <div class="row mx-auto mb-1 justify-content-between">
                            <h3 class="card-title mb-0">Pekerjaan</h3>
                            <div class="dropdown">
                              <a class="btn btn-sm btn-icon-only text-light mr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item" href="#">Terima</a>
                                <a class="dropdown-item" href="{{route('history.detail')}}">Detail</a>
                              </div>
                            </div>
                          </div>
                          <hr class="my-0 mb-2 bg-secondary"/>
                          <h5 class="card-text mb-1">Waktu Mulai : 27 Juli 2021</h5>
                          <h5 class="card-text mb-1">Waktu Selesai : 30 Juli 2021</h5>
                          <div>
                            <p class="text-muted mb-0"><small>Status</small></p>
                            <span class="badge badge-dot">
                              <i class="bg-primary"></i>
                            </span>
                            <small class="status text-muted">menunggu konfirmasi</small>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endfor
            </div>

            {{-- <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">Pekerjaan</th>
                    <th scope="col" class="sort" data-sort="budget">Waktu Mulai</th>
                    <th scope="col" class="sort" data-sort="budget1">Waktu Selesai</th>
                    <th scope="col" class="sort" data-sort="completion">Jenis Pekerjaan</th>
                    <th scope="col">Nama Pemesan</th>
                    <th scope="col" class="sort" data-sort="status">Status</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="{{asset("img/theme/bootstrap.jpg")}}">
                        </a>
                        <div class="media-body">
                          <span class="name mb-0 text-sm">Argon Design System</span>
                        </div>
                      </div>
                    </th>
                    <td class="budget">
                      25 Juli 2021
                    </td>
                    <td class="budget">
                      30 Juli 2021
                    </td>
                    <td>
                      Memasak
                    </td>
                    <td>
                      Admin
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        <i class="bg-secondary"></i>
                        <span class="status">menunggu konfirmasi</span>
                      </span>
                    </td>
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="#">Terima</a>
                          <a class="dropdown-item" href="{{route('history.detail')}}">Detail</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="{{asset("img/theme/bootstrap.jpg")}}">
                        </a>
                        <div class="media-body">
                          <span class="name mb-0 text-sm">Argon Design System</span>
                        </div>
                      </div>
                    </th>
                    <td class="budget">
                      27 Juli 2021
                    </td>
                    <td class="budget">
                      28 Juli 2021
                    </td>
                    <td>
                      Menjahit
                    </td>
                    <td>
                      Admin
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        <i class="bg-warning"></i>
                        <span class="status">pending</span>
                      </span>
                    </td>
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="#">Selesai</a>
                          <a class="dropdown-item" href="{{route('history.detail')}}">Detail</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                  
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="{{asset("img/theme/bootstrap.jpg")}}">
                        </a>
                        <div class="media-body">
                          <span class="name mb-0 text-sm">Argon Design System</span>
                        </div>
                      </div>
                    </th>
                    <td class="budget">
                      1 Juli 2021
                    </td>
                    <td class="budget">
                      8 Juli 2021
                    </td>
                    <td>
                      Menjahit
                    </td>
                    <td>
                      Admin
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        <i class="bg-success"></i>
                        <span class="status">selesai</span>
                      </span>
                    </td>
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="{{route('history.detail')}}">Detail</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
    @endsection
</x-homepage-master>