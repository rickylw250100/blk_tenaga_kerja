<x-homepage-master>
    @section('content')
    <div class="container-fluid mt--6">
        <div class="row">
          <div class="col-xl-6 order-xl-1">
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-8">
                    <h3 class="mb-0">Informasi Pekerjaan</h3>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <h5 class="text-muted col-xl-4">Jenis</h5>
                  <h5 class="col-xl-8">Menjahit</h5>                  
                </div>
                <hr class="my-2" />
                
                <div class="row">
                    <h5 class="text-muted col-xl-4">Waktu Mulai</h5>
                    <h5 class="col-xl-8">27 Juli 2021</h5>                  
                </div>
                <hr class="my-2" />
              
                <div class="row">
                    <h5 class="text-muted col-xl-4">Waktu Selesai</h5>
                    <h5 class="col-xl-8">30 Juli 2021</h5>                  
                </div>
                <hr class="my-2" />
              
                <div class="row">
                    <h5 class="text-muted col-xl-4">Status</h5>
                    <h5 class="col-xl-8">Pending</h5>                  
                </div>
                <hr class="my-2" />
              
                <div class="row">
                    <h5 class="text-muted col-xl-4">Deskripsi Pekerjaan</h5>
                    <h5 class="col-xl-8">Menjahit 3 baju</h5>                  
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-6 order-xl-1">
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-8">
                    <h3 class="mb-0">Informasi Pemesan</h3>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <h5 class="text-muted col-xl-4">Nama</h5>
                  <h5 class="col-xl-8">Ani</h5>                  
                </div>
                <hr class="my-2" />
                
                <div class="row">
                    <h5 class="text-muted col-xl-4">Alamat</h5>
                    <h5 class="col-xl-8">Alamat Pemesan</h5>                  
                </div>
                <hr class="my-2" />
              
                <div class="row">
                    <h5 class="text-muted col-xl-4">Kota</h5>
                    <h5 class="col-xl-8">Kota Pemesan</h5>                  
                </div>
                <hr class="my-2" />
              
                <div class="row">
                    <h5 class="text-muted col-xl-4">Provinsi</h5>
                    <h5 class="col-xl-8">Provinsi Pemesan</h5>                  
                </div>
                <hr class="my-2" />

              
                <div class="row">
                    <h5 class="text-muted col-xl-4">Kode Pos</h5>
                    <h5 class="col-xl-8">Kode Pos Pemesan</h5>                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                  <div class="card-header">
                    <div class="row align-items-center">
                      <div class="col-6">
                        <h3 class="mb-0">Upload Bukti Pekerjaan</h3>
                      </div>
                      <div class="col-6 text-right">
                        <a href="#!" class="btn btn-sm btn-primary">Selesai</a>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="row align-items-center">
                      <h5 class="text-muted col-3">Bukti Mulai Pekerjaan</h5>
                      <div class="text-left col-9">
                        <button class="btn btn-sm mx-2 btn-primary" type="button">
                          <span class="btn-inner--icon mr-2"><i class="ni ni-folder-17"></i></span>
                          <span class="btn-inner--text">Upload</span>
                        </button>   
                      </div>               
                    </div>
                    <hr class="my-4" />
                    
                    
                    
                    <div class="row align-items-center">
                      <h5 class="text-muted col-3">Bukti Selesai Pekerjaan</h5>
                      <div class="text-left col-9">
                        <button class="btn btn-sm mx-2 btn-primary" type="button">
                          <span class="btn-inner--icon mr-2"><i class="ni ni-folder-17"></i></span>
                          <span class="btn-inner--text">Upload</span>
                        </button>   
                      </div>               
                    </div>
                  </div>
                </div>
              </div>
        </div>
    @endsection
</x-homepage-master>