<x-homepage-master>
    @section('content')
    <div class="container-fluid mt--6">
        <div class="row">
          <div class="col-xl-4 order-xl-2">
            <div class="card card-profile">
              <img src="{{asset("img/theme/img-1-1000x600.jpg")}}" alt="Image placeholder" class="card-img-top">
              <div class="row justify-content-center">
                <div class="col-lg-3 order-lg-2">
                  <div class="card-profile-image">
                    <a href="#">
                      <img src="{{asset("img/theme/team-4.jpg")}}" class="rounded-circle">
                    </a>
                  </div>
                </div>
              </div>
              <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <div class="d-flex justify-content-between">
                </div>
              </div>
              <div class="card-body pt-0">
                <div class="row">
                  <div class="col">
                    <div class="card-profile-stats d-flex justify-content-center">
                      
                    </div>
                  </div>
                </div>
                <div class="text-center">
                  <h5 class="h3">
                    Admin<span class="font-weight-light">, 27</span>
                  </h5>
                  <div class="h5 font-weight-300">
                    <i class="ni location_pin mr-2"></i>Palembang, Sumatera Selatan
                  </div>
                  <div class="h5 mt-4">
                    <i class="ni business_briefcase-24 mr-2"></i>Bidang Pekerjaan - Penjahit
                  </div>
                  <div>
                    <i class="ni education_hat mr-2"></i>Pengalaman 1 tahun
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-8 order-xl-1">
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-4">
                    <h3 class="mb-0">Edit profile </h3>
                  </div>
                  <div class="col-8 text-right">
                    <a href="#!" class="btn btn-sm btn-primary">Ubah Password</a>
                    <a href="#!" class="btn btn-sm btn-primary">Simpan</a>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <form>
                  <h6 class="heading-small text-muted mb-4">Informasi User</h6>
                  <div class="pl-lg-4">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label" for="input-username">Username</label>
                          <input type="text" id="input-username" class="form-control" placeholder="Username" value="admin">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label" for="input-email">Email</label>
                          <input type="email" id="input-email" class="form-control" placeholder="admin@example.com">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label" for="input-first-name">Nama Depan</label>
                          <input type="text" id="input-first-name" class="form-control" placeholder="First name" value="Admin">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label" for="input-last-name">Nama Belakang</label>
                          <input type="text" id="input-last-name" class="form-control" placeholder="Last name" value="Admin">
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr class="my-4" />
                  <!-- Address -->
                  <h6 class="heading-small text-muted mb-4">Informasi Kontak</h6>
                  <div class="pl-lg-4">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="form-control-label" for="input-address">Alamat</label>
                          <input id="input-address" class="form-control" placeholder="Home Address" value="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-city">Kota</label>
                          <input type="text" id="input-city" class="form-control" placeholder="City" value="Palembang">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-country">Provinsi</label>
                          <input type="text" id="input-country" class="form-control" placeholder="Country" value="Sumatera Selatan">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-country">Kode Pos</label>
                          <input type="number" id="input-postal-code" class="form-control" placeholder="30152">
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr class="my-4" />
                  <!-- Description -->
                  <h6 class="heading-small text-muted mb-4">Tentang Saya</h6>
                  <div class="pl-lg-4">
                    <div class="form-group">
                      <label class="form-control-label">Deskripsi</label>
                      <textarea rows="4" class="form-control" placeholder="A few words about you ...">Saya adalah seorang penjahit</textarea>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    @endsection
</x-homepage-master>

